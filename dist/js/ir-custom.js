// control panel toggle (maximize/minimze)
$(".panel-toggle").on("click", function(){
	$(this).parents(".panel-control").toggleClass("is-expanded");
});

// broadcasting status
$("#broadcasting").on("click", function(){
	$(this).parents(".panel-control").toggleClass("broadcasting-is-active");
});

// switch talk volume (muted on/off)
$(".btn-talk").on("click", function(){
	$(this).toggleClass("is-muted");
});

// chat toggle (maximize/minimze)
$(".chat-toggle").on("click", function(){
	$(this).parents(".chat-box").toggleClass("is-minimized");
});

// make participant list are dragable
 $('.content-area tbody').sortable();